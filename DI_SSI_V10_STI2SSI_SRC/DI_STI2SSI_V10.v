`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Argon
// Engineer: Drozdov
// 
// Create Date:    22:21:45 12/28/2015 
// Design Name:    DI_SSI
// Module Name:    DI_STI2SSI_V10
// Project Name:   DI_STI2SSI_V10
// Target Devices: FPGA
// Tool versions:  Xilinx ISE 14.7
// Description:    Bridge STI A16D8 -> SSI
//
// Dependencies:   DI_GEN_CE_CLK, DI_WATCHDOG
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DI_STI2SSI_V10 #(
    parameter ADDR_WIDTH = 16, //Number of address bits
    SSI_CLK_DIV = 4, //dividing factor for SSI_CLK relative to CLK
    WD_MAX_COUNT = 50 //maximum watchdog cycles count before force reset
    )
    (
    //STI A16D8 signals
    input S_EX_REQ,
    output reg S_EX_ACK,
    input [ADDR_WIDTH-1:0] S_ADDR,
    input [2:0] S_CMD,
    input [7:0] S_DATA_WR,
    output reg [7:0] S_DATA_RD,
    //SSI signals
    inout SSI_DATA,
    output SSI_CLK,
    //system signals
    output reg S_ERROR,
    input CLK,
    input RST
    );
	 
localparam
   IDLE = 3'h0,
   SEND_CTRL = 3'h1,
   SEND_DATA = 3'h2,
   WAIT_FOR_WRITE_CONFIRM = 3'h3,
   WAIT_FOR_DATA = 3'h4,
   GET_DATA = 3'h5,
   END = 3'h6,
   ERROR = 3'h7;

//Internal signal declaration
wire SEND_CTRL_FINISHED_WRITE, SEND_CTRL_FINISHED_READ,
     DATA_WRITE_FINISHED, DATA_READ_FINISHED;
wire CE_FALLING, CE_RISING;
wire RST_WD;

reg [(13+ADDR_WIDTH)-1:0] CMD_REG;
reg [4:0] CT;
reg [2:0] STATE;
reg BUF_EN, RD, CW;

//Tri-state buffer
assign SSI_DATA = (BUF_EN) ? CMD_REG[0] : 1'bz;

//Counter flags
assign SEND_CTRL_FINISHED_WRITE = (CT == 5'd4 + ADDR_WIDTH);
assign SEND_CTRL_FINISHED_READ = (CT == 5'd5 + ADDR_WIDTH);
assign DATA_WRITE_FINISHED = (CT == 5'd13 + ADDR_WIDTH);
assign DATA_READ_FINISHED = (CT == 5'd12 + ADDR_WIDTH);

//CE and target clock generator
DI_GEN_CE_CLK #(
    .DIVIDE_BY(SSI_CLK_DIV)
    )
SSI_CLK_GEN (
    .CLK(CLK), 
    .RST(RST), 
    .CE_FALLING(CE_FALLING), 
	 .CE_RISING(CE_RISING),
    .SSI_CLK(SSI_CLK)
    );

//Watchdog timer
wire WD_POKE;
assign WD_POKE = (STATE == IDLE);

DI_WATCHDOG #(
    .MAX_COUNT(WD_MAX_COUNT)
    )
WATCHDOG(
    .CLK(CLK), 
    .RST(RST), 
    .CE(CE_FALLING), 
	 .POKE(WD_POKE),
    .RST_WD(RST_WD)
    );

//Control FSM
always @(posedge CLK, posedge RST)
begin
	if (RST)
	begin
		STATE <= IDLE;
	end
	else if (RST_WD)
      STATE <= ERROR;
   else
	case(STATE)
	
	IDLE:
	begin
		if(S_EX_REQ)
		STATE <= SEND_CTRL;
		else STATE <= IDLE;
	end
	
	SEND_CTRL:
	begin
      if(CE_FALLING)
         if (SEND_CTRL_FINISHED_WRITE & ~RD)
            STATE <= SEND_DATA;
         else if (SEND_CTRL_FINISHED_READ)
            STATE <= WAIT_FOR_DATA;
	end
	
	SEND_DATA:
	begin
		if (DATA_WRITE_FINISHED & CE_FALLING & CW)
         STATE <= WAIT_FOR_WRITE_CONFIRM;
      else if (DATA_WRITE_FINISHED & CE_FALLING & ~CW)
         STATE <= IDLE;
	end
   
   WAIT_FOR_WRITE_CONFIRM:
   begin
      if (~SSI_DATA & CE_RISING)
         STATE <= END;
   end
   
   WAIT_FOR_DATA:
   begin
      if (~SSI_DATA & CE_RISING)
         STATE <= GET_DATA;
   end
	
	GET_DATA:
	begin
		if (DATA_READ_FINISHED & CE_RISING)
			STATE <= END;
	end

	END:
	begin
	if (CE_FALLING)
		STATE <= IDLE;
	end
   
   ERROR:
   begin
      STATE <= IDLE;
   end

	default:
   begin
      STATE <=IDLE;
   end
	endcase
end

//Datapath
always @(posedge CLK, posedge RST)
begin
	if (RST)
	begin
		CT <= 0;
		CMD_REG <= 0;
		BUF_EN <= 1'b1;
		RD <= 1'b0;
		S_EX_ACK <= 1'b0;
		S_DATA_RD <= 8'h0;
      S_ERROR <= 1'b0;
	end
	else if (RST_WD)
   begin
		CT <= 0;
		CMD_REG <= 0;
		BUF_EN <= 1'b0;
		RD <= 1'b0;
		S_EX_ACK <= 1'b1;
		S_DATA_RD <= 8'h0;
      S_ERROR <= 1'b1;
	end
   else
	case (STATE)
	
	IDLE:
	begin
		S_EX_ACK <= 1'b0;
		CT <= 5'h0;
		BUF_EN <= 1'b0;
		if (S_EX_REQ)
		begin
         RD <= S_CMD[2];
         CW <= ~S_CMD[2] & ~S_CMD[1];
			if (S_CMD[2])
            CMD_REG <= {1'b1, S_DATA_WR, 1'b1, S_ADDR, S_CMD, 1'b0};
         else
         begin
            if (S_CMD[1])
               S_EX_ACK <= 1'b1;
            CMD_REG <= {2'b01, S_DATA_WR, S_ADDR, S_CMD, 1'b0};
         end  
		end
	end
	
	SEND_CTRL:
	begin
		S_EX_ACK <=1'b0;
		if (CE_FALLING)
		begin
         if (SEND_CTRL_FINISHED_READ)
            BUF_EN <= 1'b0;
         else
         begin
            if (~BUF_EN)
               BUF_EN <= 1'b1;
            else
               CMD_REG <= CMD_REG >> 1;
            CT <= CT + 5'h1;
         end
		end
	end
	
	WAIT_FOR_DATA:
	begin
      if(CE_FALLING)
         BUF_EN <= 1'b0;
	end
	
	SEND_DATA:
	begin
      if (CE_FALLING & DATA_WRITE_FINISHED)
         BUF_EN <= 1'b0;
      else if (CE_FALLING & ~DATA_WRITE_FINISHED)
      begin
         CMD_REG <= CMD_REG >> 1;
         CT <= CT + 5'h1;
      end
	end
   
   WAIT_FOR_WRITE_CONFIRM:
   begin
      if (~SSI_DATA & CE_RISING)
         S_EX_ACK <= 1'b1;
   end
   
	GET_DATA:
	begin
      if (CE_RISING)
      begin
         CMD_REG <= CMD_REG >> 1;
         S_DATA_RD <= S_DATA_RD >> 1;
         S_DATA_RD[7] <= SSI_DATA;
         CT <= CT + 5'h1;
         if (DATA_READ_FINISHED)
            S_EX_ACK <=1'b1;
      end
	end
	
	END:
	begin
      S_EX_ACK <=1'b0;
   end
   
   ERROR:
   begin
      S_EX_ACK <=1'b0;
      S_ERROR <= 1'b0;
   end

	endcase
end	
endmodule
