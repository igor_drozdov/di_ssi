`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Argon
// Engineer: Drozdov
// 
// Create Date:    18:23:10 03/01/2016 
// Design Name:    DI_SSI 
// Module Name:    DI_WATCHDOG
// Project Name:   
// Target Devices: FPGA, CPLD
// Tool versions:  Xilinx ISE 14.7
// Description:    Simple watchdog timer for SSI devices
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DI_WATCHDOG #(
    parameter MAX_COUNT = 50,
    CT_SIZE = log2(MAX_COUNT)
    )
    (
    input CLK,
    input RST,
	 input CE,
	 input POKE,
    output RST_WD
    );

function integer log2;
      input integer a;
      begin
         a = a - 1;
         for (log2 = 0; a > 0; log2 = log2 + 1)
            a = a >> 1;
      end
endfunction

reg [CT_SIZE-1:0] CT;
reg RG_MAX;
always @(posedge CLK, posedge RST)
begin
   if (RST)
      CT <= 0;
   else if (POKE)
      CT <= 0;
   else if (CE)
      CT <= CT + 1;
end

always @(posedge CLK, posedge RST)
begin
   if (RST)
      RG_MAX <= 1'b0;
   else
      RG_MAX <= (CT == MAX_COUNT);
end

assign RST_WD = (CT == MAX_COUNT) & ~RG_MAX;
endmodule

	


