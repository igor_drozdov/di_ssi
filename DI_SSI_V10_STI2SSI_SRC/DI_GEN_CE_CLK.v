`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Argon
// Engineer: Drozdov
// 
// Create Date:    11:57:10 01/18/2016 
// Design Name:    DI_SSI
// Module Name:    DI_GEN_CE_CLK
// Project Name:   DI_STI2SSI_V10
// Target Devices: FPGA
// Tool versions:  Xilinx ISE 14.7
// Description:    Clock generator for SSI interface with edge CE signals
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DI_GEN_CE_CLK #(
    parameter DIVIDE_BY = 2,
    CT_SIZE = log2(DIVIDE_BY)
    )
    (
    input CLK,
    input RST,
    output reg CE_FALLING,
	 output reg CE_RISING,
    output reg SSI_CLK
    );

function integer log2;
      input integer a;
      begin
         a = a - 1;
         for (log2 = 0; a > 0; log2 = log2 + 1)
            a = a >> 1;
      end
endfunction

reg [CT_SIZE - 1:0] CT;
always @(posedge CLK, posedge RST)
begin
	if (RST)
	begin
		CT <= 0;
		CE_FALLING <= 0;
	end
	
	else if (CT == (DIVIDE_BY - 1))
	begin
		CE_FALLING <= 1'b1;
		CT <= 0;
	end
	
	else
	begin
		CT <= CT + 1'b1;
		CE_FALLING <= 1'b0;
	end
end

always @(posedge CLK, posedge RST)
begin
	if (RST)
		SSI_CLK <= 1'b0;
	else if (CT == (DIVIDE_BY / 2))
		SSI_CLK <= 1'b1;
	else if (CE_FALLING)
		SSI_CLK <= 1'b0;
end

always @(posedge CLK, posedge RST)
begin
	if (RST)
		CE_RISING <= 1'b0;
	else if (CT == (DIVIDE_BY / 2 - 1))
		CE_RISING <= 1'b1;
	else
		CE_RISING <= 1'b0;
end

endmodule
