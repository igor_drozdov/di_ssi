`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   23:59:25 12/29/2015
// Design Name:   sti2ssi_top
// Module Name:   C:/Xilinx/projects/sti2ssi/sti2ssi_tb.v
// Project Name:  sti2ssi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: sti2ssi_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module sti2ssi_tb;

	// Inputs
	reg [15:0] S_ADDR;
	reg [2:0] S_CMD;
	reg [7:0] S_DATA_WR;
	reg S_EX_REQ;
	reg CLK;
	reg RST;
	reg DRIVE_DATA;
	reg TEST_DATA;

	// Outputs
	wire S_EX_ACK;
	wire [7:0] S_DATA_RD;
	wire SSI_CLK;

	// Bidirs
	wire SSI_DATA;

	// Instantiate the Unit Under Test (UUT)
	DI_STI2SSI_V11 uut (
		.S_ADDR(S_ADDR), 
		.S_CMD(S_CMD), 
		.S_DATA_WR(S_DATA_WR), 
		.S_EX_REQ(S_EX_REQ), 
		.CLK(CLK), 
		.RST(RST), 
		.S_EX_ACK(S_EX_ACK), 
		.S_DATA_RD(S_DATA_RD), 
		.SSI_CLK(SSI_CLK), 
		.SSI_DATA(SSI_DATA)
	);
	
	task WRITE_CYCLE;
		input [15:0] ADDR;
		input [2:0] CMD;
		input [7:0] DATA_WR;
		
		begin
		@(posedge CLK);
		#1;
		S_CMD = CMD;
		S_ADDR = ADDR;
		S_DATA_WR = DATA_WR;
		S_EX_REQ = 1;
      TEST_DATA = 1'b1;
         if (CMD[1] == 1)
         begin
            wait (uut.STATE == 3'h3)
            @(posedge SSI_CLK)
            DRIVE_DATA = 1'b1;
            TEST_DATA = 1'b0;
            @(posedge SSI_CLK)
            DRIVE_DATA = 1'b0;
         end
         
		wait (S_EX_ACK == 1);
      @(posedge CLK);
		S_EX_REQ = 0;
      
		end
	endtask
		
	task READ_CYCLE;
		input [15:0] ADDR;
		input [2:0] CMD;
		input [7:0] DATA_RD;
		reg [7:0] DATA_RD_REG;
		
		begin
		@(posedge CLK)
		#1
		S_CMD = CMD;
		S_ADDR = ADDR;
		DATA_RD_REG = DATA_RD;
		S_EX_REQ = 1;
		
		wait (~uut.BUF_EN & uut.STATE == 3'h4)
		@(posedge SSI_CLK)
		DRIVE_DATA = 1'b1;
		TEST_DATA = 1'b0;
		repeat(8)
		begin
			@(posedge SSI_CLK)
			TEST_DATA = DATA_RD_REG[0];
			DATA_RD_REG = DATA_RD_REG >> 1;
		end
		fork
		begin
			@(posedge SSI_CLK) TEST_DATA = 1'b1;
			@(posedge SSI_CLK) DRIVE_DATA = 1'b0;
		end
		begin
			@(posedge S_EX_ACK)
			@(posedge CLK);
			S_EX_REQ = 0;
		end
		join
		end
	endtask
	
	assign SSI_DATA = (DRIVE_DATA) ? TEST_DATA : 1'bz;
	
		initial begin
      CLK = 1'b0;
      #5;
      forever
         #(5) CLK = ~CLK;
   end
	
	initial begin
		// Initialize Inputs
		S_ADDR = 0;
		S_CMD = 0;
		S_DATA_WR = 0;
		S_EX_REQ = 0;
		RST = 1;
		DRIVE_DATA = 1'b0;

		// Wait 100 ns for global reset to finish
		#50
        
		// Add stimulus here
		RST = 0;
		#50
      WRITE_CYCLE(16'hAAAA, 3'b000, 8'h55);
      WRITE_CYCLE(16'hAAAA, 3'b010, 8'h55);
      READ_CYCLE(16'hAAAA, 3'b110, 8'h42);
		#3000
		$stop;
	end
      
endmodule

