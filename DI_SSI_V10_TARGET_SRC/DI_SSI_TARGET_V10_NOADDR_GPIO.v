`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Argon
// Engineer: Drozdov
// 
// Create Date:    13:22:41 06/03/2016 
// Design Name:    DI_SSI
// Module Name:    DI_SSI_TARGET_V10_GPIO
// Project Name:   DI_SSI_TARGET_V10
// Target Devices: FPGA, CPLD
// Tool versions:  Xilinx ISE 14.7
// Description:    Simple GPIO SSI target for no address mode
//
// Dependencies:   DI_WATCHDOG
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DI_SSI_TARGET_V10_NOADDR_GPIO
    #(parameter 
      //enabled operations
      IO_WR_EN = 1,
      MEMORY_WR_EN = 1,
      IO_RD_EN = 1,
      MEMORY_RD_EN = 1,
      PROGRAM_RD_EN = 1,
      //watchdog counts up to this value; Initiator's WD_MAX_COUNT - 2 is recommended
      WD_MAX_COUNT = 48 
    )
    (
    //SSI signals
    inout SSI_DATA,
    input SSI_CLK,
    //Data buses
    output [7:0] DATA_OUT,
    input [7:0] DATA_IN,
    //Async reset
    input RST
    );

localparam
	IDLE = 3'h0,
	GET_CTRL = 3'h1,
   SLEEP = 3'h2,
	WRITE = 3'h3,
   WRITE_CONFIRM = 3'h4,
	READ = 3'h5;
   
//signal declaration	
reg [2:0] STATE;
reg [2:0] CMD_REG;
reg [9:0] DATA_READ_REG;
reg [7:0] DATA_WRITE_REG;
reg [4:0] CT;
reg BUF_EN;

wire [2:0] CMD;
wire CW, RD, RST_WD;
wire GET_CTRL_FINISHED, WRITE_FINISHED,
     READ_FINISHED;

assign RD = CMD[2];
assign CW = ~CMD[2] & ~CMD[1];
assign DATA_OUT = DATA_WRITE_REG;
assign CMD = CMD_REG;

//CL for OP_EN signal
reg OP_EN;
always @(*)
begin
   case (CMD)
   3'b000: OP_EN = IO_WR_EN;
   3'b010: OP_EN = IO_WR_EN;
   3'b001: OP_EN = MEMORY_WR_EN;
   3'b011: OP_EN = MEMORY_WR_EN;
   3'b100: OP_EN = IO_RD_EN;
   3'b101: OP_EN = MEMORY_RD_EN;
   3'b110: OP_EN = PROGRAM_RD_EN;
   3'b111: OP_EN = PROGRAM_RD_EN;
   endcase
end

//Tri-state buf
assign SSI_DATA = (BUF_EN) ? DATA_READ_REG[0] : 1'bz;

//CT flags
assign GET_CTRL_FINISHED = (CT == 5'd4);
assign WRITE_FINISHED = (CT == 5'd12);
assign WRITE_CONFIRM_FINISHED = (CT == 5'd15);
assign READ_FINISHED = (CT == 5'd15);
assign SLEEP_FINISHED = WRITE_FINISHED;

//FSM
always @(posedge SSI_CLK, posedge RST)
begin
	if (RST)
		STATE <= IDLE;
	else if (RST_WD)
      STATE <= IDLE;
   else
   case (STATE)
	
	IDLE:
	begin
		if (~SSI_DATA)
			STATE <= GET_CTRL;
	end
	
	GET_CTRL:
	begin
      if (GET_CTRL_FINISHED & ~(OP_EN))
         STATE <= SLEEP;
		else if (GET_CTRL_FINISHED & ~RD)
			STATE <= WRITE;
      else if (GET_CTRL_FINISHED & RD)
         STATE <= READ;
	end
         
	READ:
	begin
		if (READ_FINISHED)
			STATE <= IDLE;
	end
	
	WRITE:
	begin
		if (WRITE_FINISHED & CW)
			STATE <= WRITE_CONFIRM;
      else if (WRITE_FINISHED & ~CW)
         STATE <= IDLE;
	end
   
   WRITE_CONFIRM:
   begin
      if (WRITE_CONFIRM_FINISHED)
         STATE <= IDLE;
   end
   
   SLEEP:
   begin
      if (SLEEP_FINISHED)
         STATE <= IDLE;
   end
   
endcase
end

//Datapath
always @(posedge SSI_CLK, posedge RST)
begin
	if (RST)
	begin
		BUF_EN <= 1'b0;
		CT <= 0;
		CMD_REG <= 0;
		DATA_READ_REG <= 10'b0;
      DATA_WRITE_REG <= 8'b0;
	end
	else if (RST_WD)
   begin
		BUF_EN <= 1'b0;
		CT <= 0;
		CMD_REG <= 0;
		DATA_READ_REG <= 10'b0;
      DATA_WRITE_REG <= 8'b0;
	end
   
   else case (STATE)
	
	IDLE:
	begin
		BUF_EN <= 1'b0;
		if (SSI_DATA == 1'b0)
      begin
			CT <= 5'h1;
         CMD_REG <= 0;
         DATA_READ_REG <= 10'h0;
      end
		else CT <= 5'h0;
	end
	
	GET_CTRL:
	begin
      CT <= CT + 5'h1;
      if (~GET_CTRL_FINISHED)
			CMD_REG <= {SSI_DATA, CMD_REG[2:1]};
		else if (~RD & OP_EN)
			DATA_WRITE_REG <= {SSI_DATA, DATA_WRITE_REG[7:1]};
      else if (RD & OP_EN)
         DATA_READ_REG <= {1'b1, DATA_IN, 1'b0};
	end
   
	WRITE:
	begin
      if (~WRITE_FINISHED)
         DATA_WRITE_REG <= {SSI_DATA, DATA_WRITE_REG[7:1]};
      CT <= CT + 5'h1;
	end
		
	READ:
	begin
      if (READ_FINISHED)
			BUF_EN <= 1'b0;
      else
      begin 
         if (~BUF_EN)
            BUF_EN <= 1'b1;
         else
            DATA_READ_REG <= DATA_READ_REG >> 1;	
         CT <= CT + 5'h1;
      end
	end
   
   WRITE_CONFIRM:
   begin
      if (WRITE_CONFIRM_FINISHED)
         BUF_EN <= 1'b0;
      else
      begin
         CT <= CT + 5'h1;
         if (BUF_EN == 1'b0)
            BUF_EN <= 1'b1;
         else DATA_READ_REG <= DATA_READ_REG >> 1;	
      end
   end
   
   SLEEP:
   begin
      CT <= CT + 5'h1;
   end
      
endcase
end	

//Watchdog timer
wire WD_POKE;
assign WD_POKE = (STATE == IDLE);

DI_WATCHDOG #(.MAX_COUNT(WD_MAX_COUNT))
    WATCHDOG 
    (
    .CLK(SSI_CLK), 
    .RST(RST), 
    .CE(1'b1), 
    .POKE(WD_POKE), 
    .RST_WD(RST_WD)
    );

endmodule
