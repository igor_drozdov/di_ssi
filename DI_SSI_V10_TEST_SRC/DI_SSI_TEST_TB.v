`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: Argon
// Engineer: Drozdov
//
// Create Date:   17:06:51 01/18/2016
// Design Name:   SSI_TEST_TOP
// Module Name:   S:/Logic_PRJ/Xilinx/DI_SSI_TEST/SSI_TEST_TB.v
// Project Name:  DI_SSI_TEST
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module SSI_TEST_TB;

   localparam ADDR_WIDTH = 8;
   
	// Inputs
	reg [ADDR_WIDTH-1:0] S_ADDR;
	reg [2:0] S_CMD;
	reg [7:0] S_DATA_WR;
	reg S_EX_REQ;
	reg CLK;
	reg RST;
   
   reg [7:0] DATA_IN;

	// Outputs
	wire [7:0] DATA_OUT;
	wire [7:0] S_DATA_RD;
   wire S_ERROR, S_EX_ACK;


	// Instantiate SSI slaves
   DI_SSI_TARGET_V10_RAM
    #(.SSI_ADDR_WIDTH(ADDR_WIDTH),
    .MASKED_TARGET_ADDR(8'h20),
    .IO_WR_EN(0),
    .IO_RD_EN(0),
    .MEMORY_WR_EN(1),
    .MEMORY_RD_EN(1),
    .PROGRAM_RD_EN(0)
    )
     SLAVE0 (
    .SSI_CLK(SSI_CLK), 
    .SSI_DATA(SSI_DATA),
    .RST(RST)  
    );
    
   DI_SSI_TARGET_V10_RAM
    #(.SSI_ADDR_WIDTH(ADDR_WIDTH),
    .MASKED_TARGET_ADDR(8'h30),
    .RAM_DEPTH(8),
    .IO_WR_EN(1),
    .IO_RD_EN(1),
    .MEMORY_WR_EN(0),
    .MEMORY_RD_EN(0),
    .PROGRAM_RD_EN(0)
    )
     SLAVE1 (
    .SSI_CLK(SSI_CLK), 
    .SSI_DATA(SSI_DATA),
    .RST(RST)  
    );
    
   DI_SSI_TARGET_V10_DELAY
     #(.SSI_ADDR_WIDTH(ADDR_WIDTH),
    .TARGET_ADDR(8'h42),
    .IO_WR_EN(1),
    .IO_RD_EN(1),
    .MEMORY_WR_EN(0),
    .MEMORY_RD_EN(0),
    .PROGRAM_RD_EN(0)
    )
     SLAVE2 (
    .SSI_CLK(SSI_CLK), 
    .SSI_DATA(SSI_DATA),
    .RST(RST),  
    .DATA_OUT(DATA_OUT), 
    .DATA_IN(DATA_IN)
    );
    
   //Instantiate SSI master
   DI_STI2SSI_V10 
    #(.ADDR_WIDTH(8)
    )
     MASTER (
    .S_ADDR(S_ADDR), 
    .S_CMD(S_CMD), 
    .S_DATA_WR(S_DATA_WR), 
    .S_EX_REQ(S_EX_REQ), 
    .CLK(CLK), 
    .RST(RST), 
    .S_EX_ACK(S_EX_ACK), 
    .S_DATA_RD(S_DATA_RD), 
    .SSI_CLK(SSI_CLK), 
    .SSI_DATA(SSI_DATA),
    .S_ERROR(S_ERROR)
    );
	
   task WRITE_CYCLE;
		input [ADDR_WIDTH-1:0] ADDR;
		input [2:0] CMD;
		input [7:0] DATA_WR;
		
		begin
		S_CMD = CMD;
		S_ADDR = ADDR;
		S_DATA_WR = DATA_WR;
		S_EX_REQ = 1;
      @(posedge CLK);      
		wait (S_EX_ACK);
      @(posedge CLK);
      S_EX_REQ = 0;
      $display("Wrote data %h to address %h with cmd %b", DATA_WR, ADDR, CMD);
		end
	endtask
		
   task READ_CYCLE;
		input [ADDR_WIDTH-1:0] ADDR;
		input [2:0] CMD;
		input [7:0] DATA_RD;
		
		begin
         DATA_IN = DATA_RD;
         S_CMD = CMD;
         S_ADDR = ADDR;
         S_EX_REQ = 1;
         @(posedge CLK);
         wait (S_EX_ACK);
         @(posedge CLK);
         S_EX_REQ = 0;
         $display("Read data %h from address %h with cmd %b", S_DATA_RD, ADDR, CMD);
		end
	endtask
	
   //CLK Generation
	initial
   begin
      CLK = 1'b0;
      #5;
      forever
         #(5) CLK = ~CLK;
   end
	
	initial
   begin
		// Initialize Inputs
		S_ADDR = 0;
		S_CMD = 0;
		S_DATA_WR = 0;
		DATA_IN = 0;
		S_EX_REQ = 0;
		CLK = 0;
		RST = 1;

		// Wait 100 ns for global reset to finish
		#50
      
		// Add stimulus here
		RST = 0;
		#50
      //write to memory RAM slave
      WRITE_CYCLE(8'h20, 3'b001, 8'hD0);
      WRITE_CYCLE(8'h22, 3'b011, 8'hD1);
      //read from memory RAM slave
      #2000 $display("Just waited 1000");
      READ_CYCLE(8'h20, 3'b101, 8'h00);
      READ_CYCLE(8'h22, 3'b101, 8'h00);
      //write to IO RAM slave
      WRITE_CYCLE(8'h33, 3'b000, 8'hD2);
      WRITE_CYCLE(8'h36, 3'b010, 8'hD3);
      //read from IO RAM slave
      READ_CYCLE(8'h33, 3'b100, 8'h00);
      READ_CYCLE(8'h36, 3'b100, 8'h00);
      //write to IO delay slave
      WRITE_CYCLE(8'h42, 3'b000, 8'hD5);
      $display("Delay slave output data: %h", DATA_OUT);
      //read from IO delay slave
      READ_CYCLE(8'h42, 3'b100, 8'hD5);
      $display("Delay slave input data: %h", DATA_IN);
		#3000
		$stop;
	end      
endmodule

