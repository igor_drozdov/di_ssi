`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: Argon
// Engineer: Drozdov
//
// Create Date:   18:15:53 06/03/2016
// Design Name:   DI_SSI_TARGET_SSI2STI_V10
// Module Name:   C:/Logic/DI_SSI_V10/DI_SSI_V10_Projects/DI_SSI_V10_TEST/SSI_TEST_TB2.v
// Project Name:  DI_SSI_V10_TEST
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: DI_SSI_TARGET_SSI2STI_V10
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module DI_SSI_TEST_TB2;

   localparam ADDR_WIDTH = 8,
              SLAVE_ADDR_WIDTH = 4;
   
	// Inputs
	reg [ADDR_WIDTH-1:0] S_ADDR;
	reg [2:0] S_CMD;
	reg [7:0] S_DATA_WR;
   reg [7:0] S_D_RD_SLAVE;
   reg S_EX_ACK_SLAVE;
	reg S_EX_REQ;
	reg CLK;
	reg RST;
   reg [7:0] DATA_IN;

	// Outputs
	wire [7:0] DATA_OUT;
	wire [7:0] S_DATA_RD;
   wire S_ERROR, S_EX_ACK;
   wire S_EX_fREQ_SLAVE;
   wire [SLAVE_ADDR_WIDTH-1:0] S_ADDR_SLAVE;
   wire [2:0] S_CMD_SLAVE;
   wire [7:0] S_D_WR_SLAVE;

	// Instantiate SSI slaves
   DI_SSI_TARGET_V10_SSI2STI
   #(
    .SSI_ADDR_WIDTH(ADDR_WIDTH),
    .STI_ADDR_WIDTH(SLAVE_ADDR_WIDTH),
    .MASKED_TARGET_ADDR(8'h40)
    )
     SLAVE
   (
    .SSI_DATA(SSI_DATA), 
    .SSI_CLK(SSI_CLK), 
    .S_EX_REQ(S_EX_REQ_SLAVE), 
    .S_EX_ACK(S_EX_ACK_SLAVE), 
    .S_ADDR(S_ADDR_SLAVE), 
    .S_CMD(S_CMD_SLAVE), 
    .S_D_WR(S_D_WR_SLAVE), 
    .S_D_RD(S_D_RD_SLAVE), 
    .CLK(), 
    .RST(RST)
    );
  
    
   //Instantiate SSI master
   DI_STI2SSI_V10 
    #(.ADDR_WIDTH(8)
    )
     MASTER (
    .S_ADDR(S_ADDR), 
    .S_CMD(S_CMD), 
    .S_DATA_WR(S_DATA_WR), 
    .S_EX_REQ(S_EX_REQ), 
    .CLK(CLK), 
    .RST(RST), 
    .S_EX_ACK(S_EX_ACK), 
    .S_DATA_RD(S_DATA_RD), 
    .SSI_CLK(SSI_CLK), 
    .SSI_DATA(SSI_DATA),
    .S_ERROR(S_ERROR)
    );
	
   task SSI2STI_WRITE_CYCLE;
		input [ADDR_WIDTH-1:0] ADDR;
		input [2:0] CMD;
		input [7:0] DATA_WR;
      reg ERR;
		
		begin
      $display("----SSI2STI WRITE TRANSACTION----");
		S_CMD = CMD;
		S_ADDR = ADDR;
		S_DATA_WR = DATA_WR;
      @(posedge CLK);
		#1 S_EX_REQ = 1;
      fork
         begin
            wait (S_EX_REQ_SLAVE);
            @(posedge SSI_CLK);
            S_EX_ACK_SLAVE = 1;
            @(posedge SSI_CLK);
            S_EX_ACK_SLAVE = 0;
         end
         
         begin
            wait (S_EX_ACK);
            @(posedge CLK);
            S_EX_REQ = 0;
            ERR = S_ERROR;
         end
      join
      $display("Wrote data %h to address %h with cmd %b", DATA_WR, ADDR, CMD);
      if (~ERR) $display("SSI2STI Transaction:\n S_CMD = %b\n S_ADDR = %h\n S_D_WR = %h\n",
         S_CMD_SLAVE, S_ADDR_SLAVE, S_D_WR_SLAVE);
      else
         $display("Error!");
		end
	endtask
		
   task SSI2STI_READ_CYCLE;
		input [ADDR_WIDTH-1:0] ADDR;
		input [2:0] CMD;
		input [7:0] DATA_RD;
      
      reg ERR;
		
		begin
         $display("----SSI2STI READ TRANSACTION----");
         DATA_IN = DATA_RD;
         S_CMD = CMD;
         S_ADDR = ADDR;
         @(posedge CLK);
         #1 S_EX_REQ = 1;
         fork
            begin
               wait (S_EX_REQ_SLAVE);
               @(posedge SSI_CLK);
               S_D_RD_SLAVE = DATA_RD;
               S_EX_ACK_SLAVE = 1;
               @(posedge SSI_CLK)
               S_EX_ACK_SLAVE = 0;
            end
            
            begin
               wait (S_EX_ACK);
               @(posedge CLK);
               S_EX_REQ = 0;
               ERR = S_ERROR;
            end
         join
         $display("Reading data %h from address %h with cmd %b", DATA_RD, ADDR, CMD);
         if (~ERR)
            $display("Data read: %h", S_DATA_RD);
         else
            $display("Error!");
		end
	endtask
   
   task WRITE_CYCLE;
		input [ADDR_WIDTH-1:0] ADDR;
		input [2:0] CMD;
		input [7:0] DATA_WR;
      
      reg ERR;
		
		begin
      $display("----WRITE TRANSACTION----");
		S_CMD = CMD;
		S_ADDR = ADDR;
		S_DATA_WR = DATA_WR;
		S_EX_REQ = 1;
      @(posedge CLK);      
		wait (S_EX_ACK);
      @(posedge CLK);
      S_EX_REQ = 0;
      ERR = S_ERROR;
      if (~ERR)
         $display("Wrote data %h to address %h with cmd %b", DATA_WR, ADDR, CMD);
      else
         $display("Error!");
		end
	endtask
		
   task READ_CYCLE;
		input [ADDR_WIDTH-1:0] ADDR;
		input [2:0] CMD;
		input [7:0] DATA_RD;
		
      reg ERR;
      
		begin
         $display("----READ TRANSACTION----");
         DATA_IN = DATA_RD;
         S_CMD = CMD;
         S_ADDR = ADDR;
         S_EX_REQ = 1;
         @(posedge CLK);
         wait (S_EX_ACK);
         @(posedge CLK);
         S_EX_REQ = 0;
         if (~ERR)
            $display("Read data %h from address %h with cmd %b", S_DATA_RD, ADDR, CMD);
         else
            $display("Error!");
		end
	endtask
	
   //CLK Generation
	initial
   begin
      CLK = 1'b0;
      #5;
      forever
         #(5) CLK = ~CLK;
   end
	
	initial
   begin
		// Initialize Inputs
		S_ADDR = 0;
		S_CMD = 0;
		S_DATA_WR = 0;
		DATA_IN = 0;
		S_EX_REQ = 0;
		CLK = 0;
		RST = 1;
      S_D_RD_SLAVE = 8'h0;
      S_EX_ACK_SLAVE = 1'b0;

		// Wait 100 ns for global reset to finish
		#50
      
		// Add stimulus here
		RST = 0;
		#50
      //write to memory RAM slave
      SSI2STI_WRITE_CYCLE(8'h43, 3'b010, 8'h4A);
      SSI2STI_READ_CYCLE(8'h43, 3'b100, 8'h35);
      READ_CYCLE(8'h23, 3'b100, 8'h35);
      WRITE_CYCLE(8'h23, 3'b000, 8'h4A);
		#3000
		$stop;
	end      
endmodule

