`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: Argon
// Engineer: Drozdov
//
// Create Date:   17:06:51 01/18/2016
// Design Name:   SSI_TEST_TOP
// Module Name:   DI_SSI_V10_NOADDR_TEST_TB
// Project Name:  DI_SSI_TEST
// Target Device:  
// Tool versions:  
// Description: Testbench for SSI V10 no address mode
//
// Verilog Test Fixture
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module DI_SSI_NOADDR_TEST_TB;
   
	// Inputs
	reg [2:0] S_CMD;
	reg [7:0] S_DATA_WR;
	reg S_EX_REQ;
	reg CLK;
	reg RST;
   
   reg [7:0] DATA_IN;

	// Outputs
	wire [7:0] DATA_OUT;
	wire [7:0] S_DATA_RD;
   wire S_ERROR, S_EX_ACK;


	// Instantiate SSI slaves
   DI_SSI_TARGET_V10_NOADDR_GPIO
    #(
    .IO_WR_EN(0),
    .IO_RD_EN(0),
    .MEMORY_WR_EN(1),
    .MEMORY_RD_EN(1),
    .PROGRAM_RD_EN(0)
    )
     SLAVE (
    .SSI_CLK(SSI_CLK), 
    .SSI_DATA(SSI_DATA),
    .DATA_IN(DATA_IN),
    .DATA_OUT(DATA_OUT),
    .RST(RST)  
    );
    
   //Instantiate SSI master
   DI_STI2SSI_V10_NOADDR 
     MASTER (
    .S_CMD(S_CMD), 
    .S_DATA_WR(S_DATA_WR), 
    .S_EX_REQ(S_EX_REQ), 
    .CLK(CLK), 
    .RST(RST), 
    .S_EX_ACK(S_EX_ACK), 
    .S_DATA_RD(S_DATA_RD), 
    .SSI_CLK(SSI_CLK), 
    .SSI_DATA(SSI_DATA),
    .S_ERROR(S_ERROR)
    );
	
   task WRITE_CYCLE;
		input [2:0] CMD;
		input [7:0] DATA_WR;
		
		begin
		S_CMD = CMD;
		S_DATA_WR = DATA_WR;
		S_EX_REQ = 1;
      @(posedge CLK);      
		wait (S_EX_ACK);
      @(posedge CLK);
      S_EX_REQ = 0;
      $display("Wrote data %h with cmd %b", DATA_WR, CMD);
		end
	endtask
		
   task READ_CYCLE;
		input [2:0] CMD;
		input [7:0] DATA_RD;
		
		begin
         DATA_IN = DATA_RD;
         S_CMD = CMD;
         S_EX_REQ = 1;
         @(posedge CLK);
         wait (S_EX_ACK);
         @(posedge CLK);
         S_EX_REQ = 0;
         $display("Read data %h with cmd %b", S_DATA_RD, CMD);
		end
	endtask
	
   //CLK Generation
	initial
   begin
      CLK = 1'b0;
      #5;
      forever
         #(5) CLK = ~CLK;
   end
	
	initial
   begin
		// Initialize Inputs
		S_CMD = 0;
		S_DATA_WR = 0;
		DATA_IN = 0;
		S_EX_REQ = 0;
		CLK = 0;
		RST = 1;

		// Wait 100 ns for global reset to finish
		#50
      
		// Add stimulus here
		RST = 0;
		#50
      //write to memory RAM slave
      WRITE_CYCLE(3'b001, 8'hD0);
      READ_CYCLE(3'b100, 8'h42);
		#3000
		$stop;
	end      
endmodule

