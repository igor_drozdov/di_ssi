`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:48:37 03/30/2016 
// Design Name: 
// Module Name:    DI_SSI_SIMPLE_RAM_MODULE 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DI_SSI_SIMPLE_RAM_MODULE
    #(parameter RAM_DEPTH = 4,
      ADDR_WIDTH = log2(RAM_DEPTH)
    )
    (
    input [ADDR_WIDTH - 1:0] ADDR,
    input [7:0] DATA_IN,
    output reg [7:0] DATA_OUT,
    input RW,
    input CE,
    input CLK,
    input RST
    );

function integer log2;
      input integer a;
      begin
         a = a - 1;
         for (log2 = 0; a > 0; log2 = log2 + 1)
            a = a >> 1;
      end
endfunction

reg [7:0] MEM [0:RAM_DEPTH - 1];

genvar i;
generate
   for (i=0; i < RAM_DEPTH; i=i+1) 
   begin: async_rst
      always @(posedge CLK, posedge RST)
      begin
         if (RST)
            MEM[i] <= 8'b0;
      end
   end
endgenerate

always @(posedge CLK, posedge RST)
begin
   if (~RST & CE)
   begin
      if (RW)
         DATA_OUT <= MEM[ADDR];
      else
         MEM[ADDR] <= DATA_IN;
   end
end
endmodule
