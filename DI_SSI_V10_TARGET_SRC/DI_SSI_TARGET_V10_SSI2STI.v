`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Argon
// Engineer: Drozdov
// 
// Create Date:    16:08:42 06/03/2016 
// Design Name:    DI_SSI
// Module Name:    DI_SSI_TARGET_V10_SSI2STI 
// Project Name:   DI_SSI_TEST
// Target Devices: FPGA
// Tool versions:  Xilinx ISE 14.7
// Description:    SSI target: bridge from SSI to STI
//
// Dependencies:   DI_WATCHDOG
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DI_SSI_TARGET_V10_SSI2STI
    #(parameter SSI_ADDR_WIDTH = 16,
    //number of bits in STI segment
    STI_ADDR_WIDTH = 8,
    //masked address of this target - LSB's of STI are ignored
    MASKED_TARGET_ADDR = 16'h4100,
    //watchdog counts up to this value; Initiator's WD_MAX_COUNT - 2 is recommended
    WD_MAX_COUNT = 48
    ) 
    (
    //SSI signals
    inout SSI_DATA,
    input SSI_CLK,
    //STI signals
    output reg S_EX_REQ,
    input S_EX_ACK,
    output [STI_ADDR_WIDTH-1:0] S_ADDR,
    output [2:0] S_CMD,
    output [7:0] S_D_WR,
    input [7:0] S_D_RD,
    //system signals
    output CLK,
    input RST
    );

/*function integer log2;
      input integer a;
      begin
         a = a - 1;
         for (log2 = 0; a > 0; log2 = log2 + 1)
            a = a >> 1;
      end
endfunction*/

localparam CMD_REG_MSB = (3+SSI_ADDR_WIDTH)-1;

localparam
	IDLE = 3'h0,
	GET_CTRL = 3'h1,
   WRITE_SSI = 3'h2,
	WRITE_STI = 3'h3,
   WRITE_CONFIRM = 3'h4,
	READ_STI = 3'h5,
   READ_SSI = 3'h6,
   SLEEP = 3'h7;
   
//signal declaration	
reg [2:0] STATE;
reg [CMD_REG_MSB:0] CMD_REG;
reg [9:0] DATA_READ_REG;
reg [7:0] DATA_WRITE_REG;
reg [4:0] CT;
reg BUF_EN;

//SMH FF - declared here because CPLD synthesis tool is stupid
reg SLEEP_FINISHED;

wire [2:0] CMD;
wire [SSI_ADDR_WIDTH-1:0] ADDR;
wire CW, RD, TARGET_SELECTED, RST_WD;
wire GET_CTRL_FINISHED, WRITE_FINISHED,
     READ_FINISHED;

assign RD = CMD[2];
assign CW = ~CMD[2] & ~CMD[1];
assign CMD = CMD_REG[2:0];
assign ADDR = CMD_REG[CMD_REG_MSB:3];
assign TARGET_SELECTED =
   (ADDR[SSI_ADDR_WIDTH-1:STI_ADDR_WIDTH]== MASKED_TARGET_ADDR[SSI_ADDR_WIDTH-1:STI_ADDR_WIDTH]);

assign S_ADDR = ADDR[STI_ADDR_WIDTH-1:0];
assign CLK = SSI_CLK;
assign S_D_WR = DATA_WRITE_REG;
assign S_CMD = CMD;

//Tri-state buf
assign SSI_DATA = (BUF_EN) ? DATA_READ_REG[0] : 1'bz;

//CT flags
assign GET_CTRL_FINISHED = (CT == 5'd4 + SSI_ADDR_WIDTH);
assign WRITE_FINISHED = (CT == 5'd12 + SSI_ADDR_WIDTH);
assign WRITE_CONFIRM_FINISHED = (CT == 5'd15 + SSI_ADDR_WIDTH);
assign READ_FINISHED = (CT == 5'd15 + SSI_ADDR_WIDTH);

//FSM
always @(posedge SSI_CLK, posedge RST)
begin
	if (RST)
		STATE <= IDLE;
	else if (RST_WD)
      STATE <= IDLE;
   else
   case (STATE)
	
	IDLE:
	begin
		if (~SSI_DATA)
			STATE <= GET_CTRL;
	end
	
	GET_CTRL:
	begin
      if (GET_CTRL_FINISHED & ~TARGET_SELECTED)
         STATE <= SLEEP;
		else if (GET_CTRL_FINISHED & ~RD)
			STATE <= WRITE_SSI;
      else if (GET_CTRL_FINISHED & RD)
         STATE <= READ_STI;
	end
   
	READ_SSI:
	begin
		if (READ_FINISHED)
			STATE <= IDLE;
	end
   
   READ_STI:
   begin
      if (S_EX_ACK)
         STATE <= READ_SSI;
   end
	
	WRITE_SSI:
	begin
		if (WRITE_FINISHED)
         STATE <= WRITE_STI;
	end
   
   WRITE_STI:
   begin
      if (CW & S_EX_ACK)
         STATE <= WRITE_CONFIRM;
      else if (~CW & S_EX_ACK)
         STATE <= IDLE;
   end
   
   WRITE_CONFIRM:
   begin
      if (WRITE_CONFIRM_FINISHED)
         STATE <= IDLE;
   end
   
   SLEEP:
   begin
      if (SLEEP_FINISHED)
         STATE <= IDLE;
   end
   
endcase
end

//Datapath
always @(posedge SSI_CLK, posedge RST)
begin
	if (RST)
	begin
		BUF_EN <= 1'b0;
		CT <= 0;
		CMD_REG <= 0;
		DATA_READ_REG <= 10'b0;
      DATA_WRITE_REG <= 8'b0;
      S_EX_REQ <= 1'b0;
	end
	else if (RST_WD)
   begin
		BUF_EN <= 1'b0;
		CT <= 0;
		CMD_REG <= 0;
		DATA_READ_REG <= 10'b0;
      DATA_WRITE_REG <= 8'b0;
      S_EX_REQ <= 1'b0;
	end
   
   else case (STATE)
	
	IDLE:
	begin
      S_EX_REQ <= 1'b0;
		BUF_EN <= 1'b0;
		if (SSI_DATA == 1'b0)
      begin
			CT <= 5'h1;
         CMD_REG <= 19'h0;
         DATA_READ_REG <= 10'h0;
      end
		else CT <= 5'h0;
	end
	
	GET_CTRL:
	begin
      CT <= CT + 5'h1;
      if (~GET_CTRL_FINISHED)
			CMD_REG <= {SSI_DATA, CMD_REG[CMD_REG_MSB:1]};
		else if (~RD & TARGET_SELECTED)
			DATA_WRITE_REG <= {SSI_DATA, DATA_WRITE_REG[7:1]};
      else if (RD & TARGET_SELECTED)
      begin
         S_EX_REQ <= 1'b1;
      end     
	end
   
	WRITE_SSI:
	begin
      CT <= CT + 5'h1;
      if (~WRITE_FINISHED)
         DATA_WRITE_REG <= {SSI_DATA, DATA_WRITE_REG[7:1]};
      else
      begin
         DATA_READ_REG[1:0] <= 2'b10;
         S_EX_REQ <= 1'b1;
      end
	end
   
   WRITE_STI:
   begin
      if (S_EX_ACK)
         S_EX_REQ <= 1'b0;
   end
   
   READ_STI:
   begin
      if (S_EX_ACK)
      begin
         S_EX_REQ <= 1'b0;
         DATA_READ_REG <= {1'b1, S_D_RD, 1'b0}; 
      end
   end
   
	READ_SSI:
	begin
      if (READ_FINISHED)
			BUF_EN <= 1'b0;
      else
      begin     
         if (~BUF_EN)
         begin
            BUF_EN <= 1'b1;
         end
         else
            DATA_READ_REG <= DATA_READ_REG >> 1;	
         CT <= CT + 5'h1;
      end
	end
   
   WRITE_CONFIRM:
   begin
      if (WRITE_CONFIRM_FINISHED)
         BUF_EN <= 1'b0;
      else
      begin
         CT <= CT + 5'h1;
         if (BUF_EN == 1'b0)
            BUF_EN <= 1'b1;
         else DATA_READ_REG <= DATA_READ_REG >> 1;	
      end
   end
   
   SLEEP:
   begin
   end
      
endcase
end	

//Sleep mode handler
localparam
	SMH_IDLE = 3'h0,
	SMH_COUNT = 3'h1,
	SMH_WAIT = 3'h2,
   SMH_END = 3'h3,
   DATA_WIDTH = 4'h7;

wire SMH_ENABLE;
reg [1:0] SMH_STATE;
reg [3:0] SMH_CT;

assign SMH_ENABLE = (STATE == SLEEP);

always @(posedge SSI_CLK, posedge RST)
begin
   if (RST)
   begin
      SMH_STATE <= 3'h0;
      SMH_CT <= 3'h0;
      SLEEP_FINISHED <= 1'h0;
   end
   
   else if (RST_WD)
   begin
      SMH_STATE <= 3'h0;
      SMH_CT <= 3'h0;
      SLEEP_FINISHED <= 1'h0;
   end
   
   else begin
      case (SMH_STATE)
      
      SMH_IDLE:
      begin
         SLEEP_FINISHED <= 1'b0;
         SMH_CT <=  3'h0;
         if (SMH_ENABLE)
            begin
            if (RD)
               SMH_STATE <= SMH_WAIT;
            else
            begin
               SMH_STATE <= SMH_COUNT;
               SMH_CT <= SMH_CT + 4'h1;
            end
         end
      end
      
      SMH_WAIT:
      begin
         if (~SSI_DATA & RD)
         begin
            SMH_STATE <= SMH_COUNT;
         end
         else if (~SSI_DATA & ~RD)
         begin
            SMH_STATE <= SMH_END;
            SLEEP_FINISHED <= 1'b1;
         end
      end
      
      SMH_COUNT:
      begin
         SMH_CT <= SMH_CT + 4'b1;
         if ((SMH_CT == DATA_WIDTH) & CW)
            SMH_STATE <= SMH_WAIT;
         else if ((SMH_CT == DATA_WIDTH) & ~CW)
         begin
            SMH_STATE <= SMH_END;
            SLEEP_FINISHED <= 1'b1;
         end
      end
      
      SMH_END:
      begin
         SMH_STATE <= SMH_IDLE;
         SLEEP_FINISHED <= 1'b0;
      end
      endcase
   end
end

//Watchdog timer
wire WD_POKE;
assign WD_POKE = (STATE == IDLE);

DI_WATCHDOG #(.MAX_COUNT(WD_MAX_COUNT))
    WATCHDOG
    (
    .CLK(SSI_CLK), 
    .RST(RST), 
    .CE(1'b1), 
    .POKE(WD_POKE), 
    .RST_WD(RST_WD)
    );

endmodule

