`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Argon
// Engineer: Drozdov
// 
// Create Date:    12:52:12 06/03/2016 
// Design Name:    DI_SSI
// Module Name:    DI_SSI_TARGET_V10_DELAY
// Project Name:   DI_SSI_TARGET_V10
// Target Devices: FPGA, CPLD
// Tool versions:  Xilinx ISE 14.7
// Description:    Simple SSI target with read delay emulation
//
// Dependencies:   DI_WATCHDOG
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DI_SSI_TARGET_V10_DELAY
    #(parameter 
      DELAY_CT_SIZE = 2, //width of test delay counter
      SSI_ADDR_WIDTH = 16,
      TARGET_ADDR = 16'h4101, //address of this target; full recognition
      //enabled operations
      IO_WR_EN = 1,
      MEMORY_WR_EN = 1,
      IO_RD_EN = 1,
      MEMORY_RD_EN = 1,
      PROGRAM_RD_EN = 1,
      //watchdog counts up to this value; Initiator's WD_MAX_COUNT - 2 is recommended
      WD_MAX_COUNT = 48 
    )
    (
    //SSI signals
    inout SSI_DATA,
    input SSI_CLK,
    //Data buses
    output [7:0] DATA_OUT,
    input [7:0] DATA_IN,
    //Async reset
    input RST
    );

localparam CMD_REG_MSB = (3+SSI_ADDR_WIDTH)-1;

localparam
	IDLE = 3'h0,
	GET_CTRL = 3'h1,
	WAIT = 3'h2,
	WRITE = 3'h3,
   WRITE_CONFIRM = 3'h4,
	READ = 3'h5,
   SLEEP = 3'h6;
   
//signal declaration	
reg [2:0] STATE;
reg [CMD_REG_MSB:0] CMD_REG;
reg [9:0] DATA_READ_REG;
reg [7:0] DATA_WRITE_REG;
reg [4:0] CT;
reg BUF_EN;

//SMH register - declared here because CPLD synthesis tool is stupid
reg SLEEP_FINISHED;

wire [2:0] CMD;
wire [SSI_ADDR_WIDTH-1:0] ADDR;
wire CW, RD, TARGET_SELECTED, RST_WD;
wire GET_CTRL_FINISHED, WRITE_FINISHED,
     READ_FINISHED, WAIT_FINISHED;

assign RD = CMD[2];
assign CW = ~CMD[2] & ~CMD[1];
assign DATA_OUT = DATA_WRITE_REG;
assign CMD = CMD_REG[2:0];
assign ADDR = CMD_REG[CMD_REG_MSB:3];
assign TARGET_SELECTED = (ADDR == TARGET_ADDR);

//CL for OP_EN signal
reg OP_EN;
always @(*)
begin
   case (CMD)
   3'b000: OP_EN = IO_WR_EN;
   3'b010: OP_EN = IO_WR_EN;
   3'b001: OP_EN = MEMORY_WR_EN;
   3'b011: OP_EN = MEMORY_WR_EN;
   3'b100: OP_EN = IO_RD_EN;
   3'b101: OP_EN = MEMORY_RD_EN;
   3'b110: OP_EN = PROGRAM_RD_EN;
   3'b111: OP_EN = PROGRAM_RD_EN;
   endcase
end

//Tri-state buf
assign SSI_DATA = (BUF_EN) ? DATA_READ_REG[0] : 1'bz;

//CT flags
assign GET_CTRL_FINISHED = (CT == 5'd4 + SSI_ADDR_WIDTH);
assign WRITE_FINISHED = (CT == 5'd12 + SSI_ADDR_WIDTH);
assign WRITE_CONFIRM_FINISHED = (CT == 5'd15 + SSI_ADDR_WIDTH);
assign READ_FINISHED = (CT == 5'd15 + SSI_ADDR_WIDTH);

//FSM
always @(posedge SSI_CLK, posedge RST)
begin
	if (RST)
		STATE <= IDLE;
	else if (RST_WD)
      STATE <= IDLE;
   else
   case (STATE)
	
	IDLE:
	begin
		if (~SSI_DATA)
			STATE <= GET_CTRL;
	end
	
	GET_CTRL:
	begin
      if (GET_CTRL_FINISHED & ~(TARGET_SELECTED & OP_EN))
         STATE <= SLEEP;
		else if (GET_CTRL_FINISHED & ~RD)
			STATE <= WRITE;
      else if (GET_CTRL_FINISHED & RD)
         STATE <= WAIT;
	end
   
   WAIT:
   begin
      if (WAIT_FINISHED & RD)
         STATE <= READ;
      else if (WAIT_FINISHED & ~RD)
         STATE <= WRITE_CONFIRM;
   end
         
	READ:
	begin
		if (READ_FINISHED)
			STATE <= IDLE;
	end
	
	WRITE:
	begin
		if (WRITE_FINISHED & CW)
			STATE <= WAIT;
      else if (WRITE_FINISHED & ~CW)
         STATE <= IDLE;
	end
   
   WRITE_CONFIRM:
   begin
      if (WRITE_CONFIRM_FINISHED)
         STATE <= IDLE;
   end
   
   SLEEP:
   begin
      if (SLEEP_FINISHED)
         STATE <= IDLE;
   end
   
endcase
end

//Datapath
always @(posedge SSI_CLK, posedge RST)
begin
	if (RST)
	begin
		BUF_EN <= 1'b0;
		CT <= 0;
		CMD_REG <= 0;
		DATA_READ_REG <= 10'b0;
      DATA_WRITE_REG <= 8'b0;
	end
	else if (RST_WD)
   begin
		BUF_EN <= 1'b0;
		CT <= 0;
		CMD_REG <= 0;
		DATA_READ_REG <= 10'b0;
      DATA_WRITE_REG <= 8'b0;
	end
   
   else case (STATE)
	
	IDLE:
	begin
		BUF_EN <= 1'b0;
		if (SSI_DATA == 1'b0)
      begin
			CT <= 5'h1;
         CMD_REG <= 0;
         DATA_READ_REG <= 10'h0;
      end
		else CT <= 5'h0;
	end
	
	GET_CTRL:
	begin
      CT <= CT + 5'h1;
      if (~GET_CTRL_FINISHED)
			CMD_REG <= {SSI_DATA, CMD_REG[CMD_REG_MSB:1]};
		else if (~RD & TARGET_SELECTED)
			DATA_WRITE_REG <= {SSI_DATA, DATA_WRITE_REG[7:1]};
      else if (WAIT_FINISHED & TARGET_SELECTED)
         DATA_READ_REG <= {1'b1, DATA_IN, 1'b0};
	end
   
   
   WAIT:
   begin
      if (WAIT_FINISHED & RD)
         DATA_READ_REG <= {1'b1, DATA_IN, 1'b0};
      else if (WAIT_FINISHED & ~RD)
         DATA_READ_REG[1:0] <= 2'b10;
   end
   
	WRITE:
	begin
      if (~WRITE_FINISHED)
         DATA_WRITE_REG <= {SSI_DATA, DATA_WRITE_REG[7:1]};
      CT <= CT + 5'h1;
	end
		
	READ:
	begin
      if (READ_FINISHED)
			BUF_EN <= 1'b0;
      else
      begin 
         if (~BUF_EN)
            BUF_EN <= 1'b1;
         else
            DATA_READ_REG <= DATA_READ_REG >> 1;	
         CT <= CT + 5'h1;
      end
	end
   
   WRITE_CONFIRM:
   begin
      if (WRITE_CONFIRM_FINISHED)
         BUF_EN <= 1'b0;
      else
      begin
         CT <= CT + 5'h1;
         if (BUF_EN == 1'b0)
            BUF_EN <= 1'b1;
         else DATA_READ_REG <= DATA_READ_REG >> 1;	
      end
   end
   
   SLEEP:
   begin
   end
      
endcase
end	

//Sleep mode handler
localparam
	SMH_IDLE = 3'h0,
	SMH_COUNT = 3'h1,
	SMH_WAIT = 3'h2,
   SMH_END = 3'h3,
   DATA_WIDTH = 4'h7;

wire SMH_ENABLE;
reg [1:0] SMH_STATE;
reg [3:0] SMH_CT;


assign SMH_ENABLE = (STATE == SLEEP);

always @(posedge SSI_CLK, posedge RST)
begin
   if (RST)
   begin
      SMH_STATE <= 3'h0;
      SMH_CT <= 3'h0;
      SLEEP_FINISHED <= 1'h0;
   end
   
   else if (RST_WD)
   begin
      SMH_STATE <= 3'h0;
      SMH_CT <= 3'h0;
      SLEEP_FINISHED <= 1'h0;
   end
   
   else begin
      case (SMH_STATE)
      
      SMH_IDLE:
      begin
         SLEEP_FINISHED <= 1'b0;
         SMH_CT <=  3'h0;
         if (SMH_ENABLE)
            begin
            if (RD)
               SMH_STATE <= SMH_WAIT;
            else
            begin
               SMH_STATE <= SMH_COUNT;
               SMH_CT <= SMH_CT + 4'h1;
            end
         end
      end
      
      SMH_WAIT:
      begin
         if (~SSI_DATA & RD)
         begin
            SMH_STATE <= SMH_COUNT;
         end
         else if (~SSI_DATA & ~RD)
         begin
            SMH_STATE <= SMH_END;
            SLEEP_FINISHED <= 1'b1;
         end
      end
      
      SMH_COUNT:
      begin
         SMH_CT <= SMH_CT + 4'b1;
         if ((SMH_CT == DATA_WIDTH) & CW)
            SMH_STATE <= SMH_WAIT;
         else if ((SMH_CT == DATA_WIDTH) & ~CW)
         begin
            SMH_STATE <= SMH_END;
            SLEEP_FINISHED <= 1'b1;
         end
      end
      
      SMH_END:
      begin
         SMH_STATE <= SMH_IDLE;
         SLEEP_FINISHED <= 1'b0;
      end
      endcase
   end
end

//Delay counter
reg [DELAY_CT_SIZE-1:0] WAIT_CT;

always @(posedge SSI_CLK, posedge RST)
begin
   if (RST)
      WAIT_CT <= 0;
   else if (STATE == WAIT)
      WAIT_CT <= WAIT_CT + 1;
   else
      WAIT_CT <= 0;
end
assign WAIT_FINISHED = &WAIT_CT;

//Watchdog timer
wire WD_POKE;
assign WD_POKE = (STATE == IDLE);

DI_WATCHDOG #(.MAX_COUNT(WD_MAX_COUNT))
    WATCHDOG 
    (
    .CLK(SSI_CLK), 
    .RST(RST), 
    .CE(1'b1), 
    .POKE(WD_POKE), 
    .RST_WD(RST_WD)
    );

endmodule
