`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:29:38 01/18/2016
// Design Name:   GEN_CE_CLK
// Module Name:   S:/Logic_PRJ/Xilinx/sti2ssi/GEN_CE_CLK_TB.v
// Project Name:  sti2ssi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: GEN_CE_CLK
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module GEN_CE_CLK_TB;

	// Inputs
	reg CLK;
	reg RST;

	// Outputs
	wire CE_FALLING, CE_RISING;
	wire SSI_CLK;

	// Instantiate the Unit Under Test (UUT)
	DI_GEN_CE_CLK #(
    .DIVIDE_BY(2),
    .CT_SIZE(1))
	uut(
		.CLK(CLK), 
		.RST(RST), 
		.CE_FALLING(CE_FALLING), 
		.CE_RISING(CE_RISING), 
		.SSI_CLK(SSI_CLK)
	);
	
	initial begin
	CLK = 0;
	forever #5 CLK = ~CLK;
	end

	initial begin
		// Initialize Inputs
		RST = 1;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		RST = 0;
		# 500 $stop;

	end
      
endmodule

